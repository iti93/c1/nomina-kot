package com.example.nominakotlin

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    //VARIABLES
    private var btnEntrar: Button? = null  //VARIABLES
    private var btnSalir: Button? = null
    private var txtUsuario: EditText? = null

    //componentes
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSalir = findViewById(R.id.btnSalir)
        btnEntrar = findViewById(R.id.btnEntrar)
        txtUsuario = findViewById(R.id.txtUsuario)
        btnEntrar!!.setOnClickListener{ingresar()}
        btnSalir!!.setOnClickListener{salir()}
    }
        private fun ingresar() {
                val strUsuario = txtUsuario!!.getText().toString()
                val bundle = Bundle()
                bundle.putString("usuario", strUsuario)
                val intent = Intent(this@MainActivity, ReciboNominaActivity::class.java)
                intent.putExtras(bundle)
                startActivity(intent)
            }


        private fun salir() {
            val confirmar = AlertDialog.Builder(this)
            confirmar.setTitle("Recibo de Nomina")
            confirmar.setMessage("¿Desea regresar?")
            confirmar.setPositiveButton("Confirmar") { dialogInterface: DialogInterface?, which: Int -> finish() }
            confirmar.setNegativeButton("Cancelar") { dialogInterface: DialogInterface?, which: Int ->  }
            confirmar.show()
        }
    }
