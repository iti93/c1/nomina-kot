package com.example.nominakotlin

class ReciboNomina {
    //DATOS DECLARADOS
    private var hrNormal = 0f
    private var hrExtra = 0f
    private val Subtotal = 0f
    private val Impuesto = 0f
    private var Total = 0f
    private var Puesto = 0

    //CONSTRUCTOR
    constructor(hr: Float, hrE: Float, P: Int) {
        hrNormal = hr
        hrExtra = hrE
        Puesto = P
    }

    constructor(v: Double, v1: Double) {}

    //FUNCIONES
    fun ReciboNominaC(): Float {
        var pago = 200f
        if (Puesto == 1) {
            pago *= 1.20f
            return pago
        } else if (Puesto == 2) {
            pago *= 1.50f
            return pago
        } else if (Puesto == 3) {
            pago *= 2.0f
            return pago
        }
        return pago
    }

    fun CalcularSubtotal(): Float {
        val pago = ReciboNominaC()
        val pagoExtra = pago * 2
        return pago * hrNormal + hrExtra * pagoExtra
    }

    fun CalcularImpuesto(): Float {
        val Subtotal = CalcularSubtotal()
        return Subtotal * 0.16f
    }

    fun CalcularTotal(): Float {
        val Impuesto = CalcularImpuesto()
        val Subtotal = CalcularSubtotal()
        Total = Subtotal - Impuesto
        return Total
    }
}