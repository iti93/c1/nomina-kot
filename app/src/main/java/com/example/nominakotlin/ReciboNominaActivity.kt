package com.example.nominakotlin

import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class ReciboNominaActivity : AppCompatActivity() {
    private var txtNumRecibo: EditText? = null
    private var txtNombre: EditText? = null
    private var txtHorasNormales: EditText? = null
    private var txtHorasExtra: EditText? = null
    private var rdbPuesto: RadioGroup? = null
    private var btnCalcular: Button? = null
    private var btnLimpiar: Button? = null
    private var btnRegresar: Button? = null
    private var lblSubtotalTotal: TextView? = null
    private var lblImpuestoTotal: TextView? = null
    private var lblTotalTotal: TextView? = null
    private var lblUsuario: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recibonomina)
        iniciarComponentes()

        // Obtener los datos del MainActivity
        val datos = intent.extras
        val usuario = datos!!.getString("usuario")
        lblUsuario!!.text = usuario
        btnCalcular!!.setOnClickListener {
            if (validarCampos()) {
                calcularNomina()
            } else {
                mostrarToast("Por favor, completa todos los campos")
            }
        }
        btnLimpiar!!.setOnClickListener { limpiarCampos() }
        btnRegresar!!.setOnClickListener { regresar() }
    }

    private fun iniciarComponentes() {
        txtNumRecibo = findViewById<EditText>(R.id.txtNoRecibo)
        txtNombre = findViewById<EditText>(R.id.txtNombre)
        txtHorasNormales = findViewById<EditText>(R.id.txtHr)
        txtHorasExtra = findViewById<EditText>(R.id.txtExtras)
        rdbPuesto = findViewById<RadioGroup>(R.id.rdbPuesto)
        lblSubtotalTotal = findViewById<TextView>(R.id.txtSubtotal)
        lblImpuestoTotal = findViewById<TextView>(R.id.txtImpuesto)
        lblTotalTotal = findViewById<TextView>(R.id.txtTotal)
        lblUsuario = findViewById<TextView>(R.id.lblUsuario)
        btnCalcular = findViewById<Button>(R.id.btnCalcular)
        btnLimpiar = findViewById<Button>(R.id.btnLimpiar)
        btnRegresar = findViewById<Button>(R.id.btnRegresar)
    }

    private fun validarCampos(): Boolean {
        val numRecibo = txtNumRecibo!!.text.toString()
        val nombre = txtNombre!!.text.toString()
        val horasNormales = txtHorasNormales!!.text.toString()
        val horasExtra = txtHorasExtra!!.text.toString()
        val selectedRadioButton = findViewById<RadioButton>(rdbPuesto!!.checkedRadioButtonId)
        return !numRecibo.isEmpty() && !nombre.isEmpty() && !horasNormales.isEmpty() && !horasExtra.isEmpty() && selectedRadioButton != null
    }

    private fun calcularNomina() {
        // Obtener los valores de entrada de los EditTexts
        val horasNormales = txtHorasNormales!!.text.toString().toFloat()
        val horasExtra = txtHorasExtra!!.text.toString().toFloat()

        // Obtener el texto del radio button seleccionado
        val selectedRadioButton = findViewById<RadioButton>(rdbPuesto!!.checkedRadioButtonId)
        var puesto = 0
        if (selectedRadioButton != null) {
            val puestoText = selectedRadioButton.text.toString()
            if (puestoText.equals("Auxiliar", ignoreCase = true)) {
                puesto = 1
            } else if (puestoText.equals("Albañil", ignoreCase = true)) {
                puesto = 2
            } else if (puestoText.equals("Ing. Obra", ignoreCase = true)) {
                puesto = 3
            }
        }
        val reciboNomina = ReciboNomina(horasNormales, horasExtra, puesto)

        // Realizar los cálculos necesarios en el objeto reciboNomina
        val subtotal: Float = reciboNomina.CalcularSubtotal()
        val impuesto: Float = reciboNomina.CalcularImpuesto()
        val total: Float = reciboNomina.CalcularTotal()

        // Mostrar los valores calculados en los TextViews
        lblSubtotalTotal!!.text = subtotal.toString()
        lblImpuestoTotal!!.text = impuesto.toString()
        lblTotalTotal!!.text = total.toString()
    }

    private fun limpiarCampos() {
        txtNumRecibo!!.setText("")
        txtNombre!!.setText("")
        txtHorasNormales!!.setText("")
        txtHorasExtra!!.setText("")
        rdbPuesto!!.clearCheck()
        lblSubtotalTotal!!.text = ""
        lblImpuestoTotal!!.text = ""
        lblTotalTotal!!.text = ""
    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Calculadora Nomina")
        confirmar.setMessage("Regresar al Menú Principal?")
        confirmar.setPositiveButton(
            "Confirmar"
        ) { dialog, which -> finish() }
        confirmar.setNegativeButton(
            "Cancelar"
        ) { dialog, which -> dialog.dismiss() }
        confirmar.show()
    }

    private fun mostrarToast(mensaje: String) {
        Toast.makeText(applicationContext, mensaje, Toast.LENGTH_SHORT).show()
    }
}